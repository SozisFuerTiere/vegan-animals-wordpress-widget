<?php
/*
Plugin Name: Vegan For Animals Widget
Description: Counter widget that displays how many animals are killed by the unnecessary animal agriculture and how much money farming of animals is subsidised with. <em>Please note: In this early version of the plugin, you will not yet be notified about new plugin versions. Please check the plugin page (see link below) for new versions.</em>
Text Domain: vfasft_widget_domain
Domain Path: /languages
Plugin URI: https://sozis-tiere.de/vegan-animals-wordpress-widget
Author: Moritz, Sozis für Tiere
Version: 2021-01-18
Author URI: https://sozis-tiere.de/
*/

if(!defined('ABSPATH')){
    exit;
}
  
require_once(plugin_dir_path(__FILE__).'/includes/veganforanimals-scripts.php');

// Register and load the widget 
function vfasft_load_widget() {
    register_widget( 'vfasft_widget' );
}
/**
 * Load plugin textdomain.
 */
function vfasft_load_textdomain() {
    load_plugin_textdomain( 'vfasft_widget_domain', false, dirname( plugin_basename( __FILE__ ) ) . '/languages');
}

add_action( 'widgets_init', 'vfasft_load_widget' );
add_action( 'init', 'vfasft_load_textdomain' );

class vfasft_widget extends WP_Widget {
 
function __construct() {
parent::__construct(
 
// Base ID of your widget
'vfasft_widget', 
 
// Widget name that will appear in UI
__('Vegan For Animals Widget', 'vfasft_widget_domain'), 
 
// Widget description
array( 'description' => __( 'Displays information about animal agriculture.', 'vfasft_widget_domain' ), ) 
);
}

private $time_range_options = [
    'live'      => 1,
    'second'    => 0,
    'day'       => 1,
    'week'      => 0,
    'month'     => 1,
    'year'      => 1,
];

public function unique( $id ){
    // This serves basically the same purpose as $this->get_field_id(), only it replaces
    // dashes by underscores.
    // Replacing dashes is a bit of a hack, but the widget ID which also contains a dash
    // followed by a number will be exported to JavaScript as part of variable names.
    // In JS a variable name with a dash followed by a number will throw a synax error.
    return $id . '_' . str_replace('-', '_', $this->id);
}

public function returnTranslationStrings( $key ){
    switch( $key ){
        case 'live': return __( 'Live' , 'vfasft_widget_domain'); break;
        case 'second': return __( 'Per second' , 'vfasft_widget_domain'); break;
        case 'day': return __( 'Per day' , 'vfasft_widget_domain'); break;
        case 'week': return __( 'Per week' , 'vfasft_widget_domain'); break;
        case 'month': return __( 'Per month' , 'vfasft_widget_domain'); break;
        case 'year': return __( 'Per year' , 'vfasft_widget_domain'); break;
        default: return __( '' , 'vfasft_widget_domain'); break;
    }
}
 
public function widget( $args, $instance ) {
echo $args['before_widget'];

if ( ! empty($instance['title'] ) ){
    $title = apply_filters( 'widget_title', $instance['title'] );
    echo $args['before_title'] . $title . $args['after_title'];
}

//TODO Avoid duplicate variable declarations (make it a global array?)
$options = [
    'mode',
    'region',
    'refreshrate',
    'annotation',
    'display_source_domain',
    'display_credits',
    'call_to_action',
    'cta_style',
    'cta_custom_class',
    'actionlink',
    'display_compact_number',
];

foreach ($options as $option){
    extract([$option => $instance[$option]]);
}

$selected_time_options = 0;
foreach(array_keys($this->time_range_options) as $option){
    if($instance[$option] == 1){
        $selected_time_options++;
    }
}
if($selected_time_options == 0){
    $instance['live'] = true;
}

$animals = [
"wild_caught_fish" => __('wild caught fish', 'vfasft_widget_domain'),
"chickens" => __('chickens', 'vfasft_widget_domain'),
"farmed_fish" => __('farmed fish', 'vfasft_widget_domain'),
"ducks" => __('ducks', 'vfasft_widget_domain'),
"pigs" => __('pigs', 'vfasft_widget_domain'),
"rabbits" => __('rabbits', 'vfasft_widget_domain'),
"geese" => __('geese', 'vfasft_widget_domain'),
"turkeys" => __('turkeys', 'vfasft_widget_domain'),
"sheep" => __('sheep', 'vfasft_widget_domain'),
"goats" => __('goats', 'vfasft_widget_domain'),
"cattle" => __('cattle', 'vfasft_widget_domain'),
"rodents" => __('rodents', 'vfasft_widget_domain'),
"other_birds" => __('other birds', 'vfasft_widget_domain'),
"buffalo" => __('buffalo', 'vfasft_widget_domain'),
"horses" => __('horses', 'vfasft_widget_domain'),
"camels" => __('camels', 'vfasft_widget_domain'),
"donkeys" => __('donkeys', 'vfasft_widget_domain'),
];

echo '<div class="' . $mode . '" id="' . $this->unique("") . '">';
echo '<div class="vfasft-header-section">';

$region_translated = 'hans';

switch($region){
    case 'vfasft-worldwide': $region_translated = __('worldwide', 'vfasft_widget_domain'); break;
    case 'vfasft-germany':   $region_translated = __('in Germany', 'vfasft_widget_domain'); break;
    default:          $region_translated = '';
}

switch($mode){
    case 'vfasft-killcounter':
        $introduction_live =   sprintf(__('This is how many animals have been killed %s by the unnecessary animal agriculture since you opened this webpage <span class="vfasft-clock"><span class="vfasft-clock-hours"><span class="vfasft-clock-hours-number">0</span> hour(s) </span><span class="vfasft-clock-minutes"><span class="vfasft-clock-minutes-number">0</span> minute(s) </span><span class="vfasft-clock-seconds"><span class="vfasft-clock-seconds-number">0</span> seconds</span></span> ago:', 'vfasft_widget_domain'), $region_translated);
        $introduction_second = sprintf(__('This is how many animals are killed %s by the unnecessary animal agriculture each second:', 'vfasft_widget_domain'), $region_translated);
        $introduction_day =    sprintf(__('This is how many animals are killed %s by the unnecessary animal agriculture each day:', 'vfasft_widget_domain'), $region_translated);
        $introduction_week =   sprintf(__('This is how many animals are killed %s by the unnecessary animal agriculture each week:', 'vfasft_widget_domain'), $region_translated);
        $introduction_month =  sprintf(__('This is how many animals are killed %s by the unnecessary animal agriculture each month:', 'vfasft_widget_domain'), $region_translated);
        $introduction_year =   sprintf(__('This is how many animals are killed %s by the unnecessary animal agriculture each year:', 'vfasft_widget_domain'), $region_translated);
    break;
    case 'vfasft-subsidies':
        $introduction_live =   __('This is the amount of money the EU has supported the suffering in the unnecessary animal agriculture with since you opened this webpage <span class="vfasft-clock"><span class="vfasft-clock-hours"><span class="vfasft-clock-hours-number">0</span> hour(s) </span><span class="vfasft-clock-minutes"><span class="vfasft-clock-minutes-number">0</span> minute(s) </span><span class="vfasft-clock-seconds"><span class="vfasft-clock-seconds-number">0</span> seconds</span></span> ago:', 'vfasft_widget_domain');
        $introduction_second = __('This is the amount of money the EU supports the suffering in the unnecessary animal agriculture with each second:', 'vfasft_widget_domain');
        $introduction_day =    __('This is the amount of money the EU supports the suffering in the unnecessary animal agriculture with each day:', 'vfasft_widget_domain');
        $introduction_week =   __('This is the amount of money the EU supports the suffering in the unnecessary animal agriculture with each week:', 'vfasft_widget_domain');
        $introduction_month =  __('This is the amount of money the EU supports the suffering in the unnecessary animal agriculture with each month:', 'vfasft_widget_domain');
        $introduction_year =   __('This is the amount of money the EU supports the suffering in the unnecessary animal agriculture with each year:', 'vfasft_widget_domain');
    break;
    default:
        $introduction_live = '';
        $introduction_second = '';
        $introduction_day = '';
        $introduction_week = '';
        $introduction_month = '';
        $introduction_year = '';
    break;
}
?>

<div class="vfasft-tab vfasft-tab-live" id="<?php echo $this->unique("vfasft-tab-content-live")?>">
<p><?php echo $introduction_live; ?></p>
</div>
<div class="vfasft-tab vfasft-tab-second" id="<?php echo $this->unique("vfasft-tab-content-second")?>">
<p><?php echo $introduction_second; ?></p>
</div>
<div class="vfasft-tab vfasft-tab-day" id="<?php echo $this->unique("vfasft-tab-content-day")?>">
<p><?php echo $introduction_day; ?></p>
</div>
<div class="vfasft-tab vfasft-tab-week" id="<?php echo $this->unique("vfasft-tab-content-week")?>">
<p><?php echo $introduction_week; ?></p>
</div>
<div class="vfasft-tab vfasft-tab-month" id="<?php echo $this->unique("vfasft-tab-content-month")?>">
<p><?php echo $introduction_month; ?></p>
</div>
<div class="vfasft-tab vfasft-tab-year" id="<?php echo $this->unique("vfasft-tab-content-year")?>">
<p><?php echo $introduction_year; ?></p>
</div>
</div>

<div class="vfasft-container">
<div id="<?php echo $this->unique("vfasft-list-container")?>" class="vfasft-borders">

<?php
if(strcmp($mode, "vfasft-killcounter") == 0){
    echo '<ul class="vfasft-list" id="' .  $this->unique('vfasft-list') . '">';
    foreach ($animals as $key => $nice_string){
        echo '<li class="vfasft-row-container" id="' .  $this->unique('vfasft-row-' . $key) . '">';
        echo '<div class="vfasft-number-container">';
        echo '<span class="vfasft-number" id="' . $this->unique('vfasft-' . $key) . '">0</span>';
        if($display_compact_number == 1){
            echo '<span class="vfasft-number-compact" id="' . $this->unique('vfasft-' . $key . '-compact') . '"></span>';
        }
        echo '</div>';
        echo '<span class="vfasft-animal-names">' . $nice_string . '</span>';
        echo '</li>';
    }
    echo '</ul>';
}
else if(strcmp($mode, "vfasft-subsidies") == 0){
    echo '<div class="vfasft-subsidies-container">';
    echo '<div class="vfasft-subsidies-number-container">';
    echo '<span class="vfasft-subsidies-number" id="' . $this->unique('vfasft-subsidies-number') . '">0</span>';
    echo $display_compact_number == 1 ? '<span class="vfasft-subsidies-number-compact" id="' . $this->unique('vfasft-subsidies-compact') . '"></span>' : '';
    echo '</div>';
    echo '<span class="vfasft-subsidies-number vfasft-currency">€</span>';
    echo '</div>';
}

//echo '</div>';

/*echo '<table class="vfasft_table">';
foreach ($animals as $key => $nice_string){
  echo '<tr id="vfasft-row-' . $key . '"><td class="vfasft-number"><span id="vfasft-' . $key . '" class="vfasft-number">0</span></td><td class="vfasft-animal-names"> ' . $nice_string . '</td></tr>';
}
echo '</table>';*/

/*echo '<ul class="vfasft-floatingboxes">';
foreach ($animals as $key => $nice_string){
  echo '<li id="vfasft-li-' . $key . '" class="vfasft-borders"><span class="vfasft-number" id="vfasft-' . $key . '">0</span><p><span class="vfasft-animal-names">' . $nice_string . '</span></p></li>';
}
echo '</ul>';*/

$sourcestring = __('Source', 'vfasft_widget_domain');
if($display_source_domain == 1){
    $sourcestring .= ': <span id="' . $this->unique('vfasft-sourcedomain') . '"></span>';
}

echo '<a class="vfasft-source" id="' . $this->unique('vfasft-sourceanchor') . '" title="" target="_blank" rel="nofollow noopener noreferrer" href="">' . $sourcestring . '</a>';
echo '</div>';

echo '<div class="vfasft-tab-bar">';

$first_radiobox = true;
foreach(array_keys($this->time_range_options) as $option){
    if($instance[$option]){
        echo '<input type="radio" name="' . $this->unique("vfasft-tabs") . '" class="vfasft-input vfasft-tab-' . $option . '" data-widget-id="' . $this->unique("") . '" id="' . $this->unique('vfasft-tab-' . $option) . '"' . ($first_radiobox ? ' checked' : '') . '/>';
        $first_radiobox = false;
        echo '<label for="' . $this->unique('vfasft-tab-' . $option) . '" ' . ($selected_time_options < 2 ? 'class="hidden"' : '') . '>' . ($option == 'live' ? '<span style="color: red">&#9679;</span> ' : ' ') . $this->returnTranslationStrings($option) . '</label>';
    }
}

echo '</div><br/>';
echo '<div class="vfasft-annotation">';

$cta_class = "";
switch($cta_style){
    case "vfasft-cta-button": $cta_class .= "vfasft-button"; break;
    case "vfasft-cta-link": break;
    case "vfasft-cta-custom": $cta_class .= $cta_custom_class; break;
    default: break;
}

echo $annotation . ' <a href="' . $actionlink . '"><span class="' . $cta_class . '">' . $call_to_action . '</span></a></div>';

wp_enqueue_script('vfasft-count-script');
wp_localize_script( 'vfasft-count-script', $this->unique('vfasft_settings'), array(
    'refreshrate' => $refreshrate,
    'region' => $region
));

if($display_credits == 1){
    echo '<div class="vfasft-credits">';
    echo '<a id="' . $this->unique('vfasft-credits-widgetname') . '" target="_blank" rel="nofollow noopener noreferrer" href="https://sozis-tiere.de/vegan-animals-wordpress-widget">Vegan For Animals WordPress Widget</a>
 by <a id="' . $this->unique('vfasft-credits-creator') . '" target="_blank" rel="nofollow noopener noreferrer" href="https://sozis-tiere.de/">Sozis für Tiere</a>';
    echo '</div>';
}

echo '</div></div>';
echo $args['after_widget'];
}

// Widget Backend 
public function form( $instance ) {
    $defaults = [
        'mode'                  => 'vfasft-killcounter',
        'region'                => 'worldwide',
        'title'                 => __( 'Animal killings', 'vfasft_widget_domain' ),
        'refreshrate'           => 10,
        'annotation'            => __( 'You want to stop this?', 'vfasft_widget_domain' ),
        'display_source_domain' => 1,
        'call_to_action'        => __( 'Take action!', 'vfasft_widget_domain' ),
        'actionlink'            => 'https://sozis-tiere.de',
        'cta_style'             => 'vfasft-cta-button',
        'cta_custom_class'      => '',
        'display_compact_number'=> 1, //Interpreted as True
        'display_credits'       => 1, //Interpreted as True
    ];

    $defaults = array_merge($defaults, $this->time_range_options);
    extract( wp_parse_args( ( array ) $instance, $defaults ) );
?>

<label for="<?php echo $this->get_field_id( 'mode' ); ?>"><?php _e( 'Mode:', 'vfasft_widget_domain'); ?></label>
<select
    class="widefat vfasft-select vfasft-mode-select"
    id="<?php echo $this->get_field_id( 'mode' ); ?>" 
    name="<?php echo $this->get_field_name( 'mode' ); ?>"  
    value="<?php echo esc_attr( $mode ); ?>" 
    >
    <option value="vfasft-killcounter" id="slct_killcounter" <?php echo selected( $mode, 'vfasft-killcounter', false ) ?> ><?php _e('Animal killings', 'vfasft_widget_domain')?></option>
    <option value="vfasft-subsidies" id="slct_subsidies" <?php echo selected( $mode, 'vfasft-subsidies', false ) ?> ><?php _e('Subsidies', 'vfasft_widget_domain')?></option>
</select>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:'); ?></label> 
<input 
    class="widefat"
    id="<?php echo $this->get_field_id( 'title' ); ?>" 
    name="<?php echo $this->get_field_name( 'title' ); ?>" 
    type="text" 
    value="<?php echo esc_attr( $title ); ?>" 
    />

<label class="killcounter-only" for="<?php echo $this->get_field_id( 'region' ); ?>"><?php _e( 'Region:'); ?></label>
<select
    class="widefat vfasft-select vfasft-region-select killcounter-only"
    id="<?php echo $this->get_field_id( 'region' ); ?>" 
    name="<?php echo $this->get_field_name( 'region' ); ?>"  
    value="<?php echo esc_attr( $region ); ?>" 
    >
    <option value="vfasft-worldwide" id="slct_worldwide" <?php echo selected( $region, 'vfasft-worldwide', false ) ?> ><?php _e('worldwide', 'vfasft_widget_domain')?></option>
    <option value="vfasft-germany" id="slct_germany" <?php echo selected( $region, 'vfasft-germany', false ) ?> ><?php _e('Germany', 'vfasft_widget_domain')?></option>
</select>

<?php
echo '<fieldset><legend>Interface</legend>';
echo '<p>' . _e('Which display options should the viewer be able to choose from?', 'vfasft_widget_domain') . '</p>';
echo '<ul>';
foreach($this->time_range_options as $key => $value){
    echo '<li><input
    class="widefat"
    id="' . $this->get_field_id( $key ) . '"
    name="' . $this->get_field_name( $key ) . '"
    type="checkbox" ';
    checked('1', ${$key} ); // This operator references a variable by its name as provided by a string (which is what $key contains)
    echo '/>';
    echo '<label for="' . $this->get_field_id( $key ) . '">' . $this->returnTranslationStrings($key) . '</label></li>'; 
}
echo '</ul>';
?>
<label for="<?php echo $this->get_field_id( 'refreshrate' ); ?>"><?php _e( 'Refresh rate per second (Default: 10)' , 'vfasft_widget_domain'); ?></label>
<input
    class="widefat"
    id="<?php echo $this->get_field_id( 'refreshrate' ); ?>"
    name="<?php echo $this->get_field_name( 'refreshrate' ); ?>"
    type="number"
    value="<?php echo esc_attr( $refreshrate ); ?>"
    min="0.001"
    max="20"
    step="any"
    />
<p>
<input 
    class="widefat"
    id="<?php echo $this->get_field_id( 'display_source_domain' ); ?>"
    name="<?php echo $this->get_field_name( 'display_source_domain' ); ?>"
    type="checkbox"
    <?php checked('1', $display_source_domain) ?>
    />
    <label for="<?php echo $this->get_field_id( 'display_source_domain' ); ?>"><?php _e( 'Display domain name of source' , 'vfasft_widget_domain'); ?></label>
</p>
<input 
    class="widefat"
    id="<?php echo $this->get_field_id( 'display_credits' ); ?>"
    name="<?php echo $this->get_field_name( 'display_credits' ); ?>"
    type="checkbox"
    <?php checked('1', $display_credits) ?>
    />
    <label for="<?php echo $this->get_field_id( 'display_credits' ); ?>"><?php _e( "Display widget promotion" , 'vfasft_widget_domain'); ?></label>

</fieldset>

<fieldset><legend><?php _e('Call to action', 'vfasft_widget_domain')?></legend>
<label for="<?php echo $this->get_field_id( 'annotation' ); ?>"><?php _e( 'Annotation:' , 'vfasft_widget_domain'); ?></label> 
<input 
    class="widefat" 
    id="<?php echo $this->get_field_id( 'annotation' ); ?>" 
    name="<?php echo $this->get_field_name( 'annotation' ); ?>" 
    type="text" 
    value="<?php echo esc_attr( $annotation ); ?>" 
    />
    <label for="<?php echo $this->get_field_id( 'call_to_action' ); ?>"><?php _e( 'Label of button:' , 'vfasft_widget_domain'); ?></label> 
<input
    class="widefat"
    id="<?php echo $this->get_field_id( 'call_to_action' ); ?>"
    name="<?php echo $this->get_field_name( 'call_to_action' ); ?>"
    type="text"
    value="<?php echo esc_attr( $call_to_action); ?>"
    />
<label for="<?php echo $this->get_field_id( 'actionlink' ); ?>"><?php _e( 'Link:' , 'vfasft_widget_domain'); ?></label> 
<input 
    class="widefat"
    id="<?php echo $this->get_field_id( 'actionlink' ); ?>"
    name="<?php echo $this->get_field_name( 'actionlink' ); ?>"
    type="text"
    value="<?php echo esc_attr( $actionlink); ?>"
    />
<label for="<?php echo $this->get_field_id( 'cta_style' ); ?>"><?php _e( 'Style:', 'vfasft_widget_domain'); ?></label>
<select
    class="widefat vfasft-select vfasft-cta-style-select"
    id="<?php echo $this->get_field_id( 'cta_style' ); ?>"
    name="<?php echo $this->get_field_name( 'cta_style' ); ?>"
    value="<?php echo esc_attr( $cta_style ); ?>"
    >
    <option value="vfasft-cta-button" id="slct_cta_button" <?php echo selected( $cta_style, 'vfasft-cta-button', false ) ?> ><?php _e('Button', 'vfasft_widget_domain')?></option>
    <option value="vfasft-cta-link" id="slct_cta_link" <?php echo selected( $cta_style, 'vfasft-cta-link', false ) ?> ><?php _e('Simple link', 'vfasft_widget_domain')?></option>
    <option value="vfasft-cta-custom" id="slct_cta_custom" <?php echo selected( $cta_style, 'vfasft-cta-custom', false ) ?> ><?php _e('Custom', 'vfasft_widget_domain')?></option>
</select>
<label class="cta-custom-only" for="<?php echo $this->get_field_id( 'cta_custom_class' ); ?>"><?php _e( 'Custom CSS class:' , 'vfasft_widget_domain'); ?></label>
<input
    class="widefat cta-custom-only"
    id="<?php echo $this->get_field_id( 'cta_custom_class' ); ?>"
    name="<?php echo $this->get_field_name( 'cta_custom_class' ); ?>"
    type="text"
    value="<?php echo esc_attr( $cta_custom_class); ?>"
    />
</fieldset>

<fieldset class="subsidies-only"><legend><?php _e('Subsidies', 'vfasft_widget_domain')?></legend>
<input 
    class="widefat"
    id="<?php echo $this->get_field_id( 'display_compact_number' ); ?>"
    name="<?php echo $this->get_field_name( 'display_compact_number' ); ?>"
    type="checkbox"
    <?php checked('1', $display_compact_number) ?>
    />
    <label for="<?php echo $this->get_field_id( 'display_compact_number' ); ?>"><?php _e( 'Display easily readable compact number' , 'vfasft_widget_domain'); ?></label>
</fieldset>

<?php
}
     
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['mode']                   = $new_instance['mode'];
$instance['title']                  = strip_tags( $new_instance['title'] );
$instance['region']                 = $new_instance['region'];
$instance['refreshrate']            = strip_tags( $new_instance['refreshrate'] );
$instance['annotation']             = strip_tags( $new_instance['annotation'] );
$instance['display_source_domain']  =  ( ! empty( $new_instance['display_source_domain'] ) ) ? 1 : 0;
$instance['display_credits']        =  ( ! empty( $new_instance['display_credits'] ) ) ? 1 : 0;
$instance['call_to_action']         = strip_tags( $new_instance['call_to_action'] );
$instance['cta_style']              = strip_tags( $new_instance['cta_style'] );
$instance['cta_custom_class']       = strip_tags( $new_instance['cta_custom_class'] );
$instance['actionlink']             = strip_tags( $new_instance['actionlink'] );
$instance['display_compact_number'] =  ( ! empty( $new_instance['display_compact_number'] ) ) ? 1 : 0;

foreach(array_keys($this->time_range_options) as $option){
    $instance[$option] = ( ! empty($new_instance[ $option ]) ) ? 1 : 0;
}

return $instance;
}
}

?>
