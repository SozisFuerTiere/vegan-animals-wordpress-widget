# Vegan For Animals WordPress Widget

## What does this widget do?
How many animals are being killed as you're reading this? How is that supported by states or the European Union? Our free (and open source) WordPress widget makes these issues transparent to the broader public.

For more information and installation instructions, visit: https://sozis-tiere.de/vegan-animals-wordpress-widget
