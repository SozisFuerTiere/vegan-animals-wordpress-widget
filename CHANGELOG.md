# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [beta1]
### Added
This is the first release to the public. The plugin contains:

- an animal kill counter
  - with worldwide data
  - with data for Germany
- a subsidies counter with data for the EU

The plugin is available in German and English.
