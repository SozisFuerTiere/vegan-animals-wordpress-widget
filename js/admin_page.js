jQuery(document).ready(function ($) {
    // Set correct states at startup
    $('select.vfasft-select').each(function () {
        hideAndShowOptions(this);
        updateWidgetTitle();
    });

    // Update states with every <select> change
    $('.widget-liquid-right').on('change', 'select.vfasft-select', function () {
        hideAndShowOptions(this);
        updateWidgetTitle(this);
    });
});

function hideAndShowOptions(element) {
    switch (element.value) {
        case 'vfasft-killcounter':
            jQuery(element).siblings('.subsidies-only').hide();
            jQuery(element).siblings('.killcounter-only').show();
            break;
        case 'vfasft-subsidies':
            jQuery(element).siblings('.killcounter-only').hide();
            jQuery(element).siblings('.subsidies-only').show();
            break;
        case 'vfasft-cta-button':
        case 'vfasft-cta-link':
            jQuery(element).siblings('.cta-custom-only').hide();
            break;
        case 'vfasft-cta-custom':
            jQuery(element).siblings('.cta-custom-only').show();
            break;
        default:
            break;
    }
}

function updateWidgetTitle(element) {
    let mode = jQuery(element).parent().children('select[id$="mode"]').children("option:selected").text();
    let region = jQuery(element).parent().children('select[id$="region"]').children("option:selected").text();
    let title = "";

    if (jQuery(element).hasClass('vfasft-mode-select')) {
        switch (element.value) {
            case 'vfasft-killcounter': title = mode + ' (' + region + ')'; break;
            case 'vfasft-subsidies':   title = mode; break;
            default: break;
        }
    }
    else if (jQuery(element).hasClass('vfasft-region-select')){
        title = mode + ' (' + region + ')';
    };

    jQuery(element).siblings('input[id$="title"]').val(title);

}
