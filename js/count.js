// Enter the relevant data below.
// By default, the worldwide figures are provided.
const animalsKilledPerYearWorldwide = {
    "wild_caught_fish": 970000000000,
    "chickens": 61171973510,
    "farmed_fish": 38000000000,
    "ducks": 2887594480,
    "pigs": 1451856889,
    "rabbits": 1171578000,
    "geese": 687147000,
    "turkeys": 618086890,
    "sheep": 536742256,
    "goats": 438320371,
    "cattle": 298799160,
    "rodents": 70371000,
    "other_birds": 59656000,
    "buffalo": 25798819,
    "horses": 4863367,
    "donkeys": 3213400,
    "camels": 3243266,
};
const killcounterWorldwideSource = 'https://considerveganism.com/counter/';

const animalsKilledPerYearGermany = {
    "wild_caught_fish": 0,
    "chickens": 652696800,
    "farmed_fish": 0,
    "ducks": 15876488,
    "pigs": 55194809,
    "rabbits": 0,
    "geese": 593960,
    "turkeys": 34226003,
    "sheep": 1149383,
    "goats": 24715,
    "cattle": 3412207,
    "rodents": 0,
    "other_birds": 6125,
    "buffalo": 0,
    "horses": 5113,
    "donkeys": 0,
    "camels": 0,
};
const killcounterGermanySource = 'https://www.destatis.de/DE/Presse/Pressemitteilungen/2020/02/PD20_036_413.html';

const subsidiesPerYear = 30000000000;
const subsidiesSource = 'https://www.greenpeace.de/presse/publikationen/feeding-problem';

const SECONDS_PER_DAY = 24 * 60 * 60;
const SECONDS_PER_WEEK = SECONDS_PER_DAY * 7;
const SECONDS_PER_YEAR = 365 * SECONDS_PER_DAY;
const SECONDS_PER_MONTH = SECONDS_PER_YEAR / 12;
let startTime;
let count = 0;
let widgetInstances = {};

window.addEventListener('DOMContentLoaded', function () {
    startTime = new Date().getTime();

    jQuery(".vfasft-killcounter").each(function () {
        let id = jQuery(this).attr('id');
        widgetInstances[id] = { mode: 'vfasft-killcounter' };

        switch (window['vfasft_settings' + id].region) {
            case 'vfasft-germany': source = killcounterGermanySource; break;
            default: source = killcounterWorldwideSource; break;
        }
        jQuery('#vfasft-sourceanchor' + id).attr('href', source);
        jQuery('#vfasft-sourceanchor' + id).attr('title', source);
        let domain = source.replace('http://','').replace('https://','').split(/[/?#]/)[0];
        jQuery('#vfasft-sourcedomain' + id).text(domain);
    });

    jQuery(".vfasft-subsidies").each(function () {
        let id = jQuery(this).attr('id');
        widgetInstances[id] = { mode: 'vfasft-subsidies' };
        source = subsidiesSource;
        jQuery('#vfasft-sourceanchor' + id).attr('href', source);
        jQuery('#vfasft-sourceanchor' + id).attr('title', source);
        let domain = source.replace('http://','').replace('https://','').split(/[/?#]/)[0];
        jQuery('#vfasft-sourcedomain' + id).text(domain);
    });

    jQuery("input.vfasft-input[checked]").each(function () {
        timerangeSelected(jQuery(this));
    });
});

function chooseAndStartTimer(widgetId) {
    const instance = widgetInstances[widgetId];
    let func;
    switch (instance.mode) {
        case 'vfasft-killcounter': func = updateKillCounterLive; break;
        case 'vfasft-subsidies': func = updateSubsidiesCounterLive; break;
        default: break;
    }
    // Run update function once right away because if the refreshrate is low,
    // it will take a while for the first update to appear on the screen.
    func(widgetId);
    instance.intervalHandle = window.setInterval(func, 1000 / window['vfasft_settings' + widgetId].refreshrate, widgetId);
}

function updateKillCounterLive(widgetId = "") {
    const secondsSincePageload = (new Date().getTime() - startTime) / 1000;
    updateKillCounter(secondsSincePageload, widgetId);
}

function updateSubsidiesCounterLive(widgetId = "") {
    const secondsSincePageload = (new Date().getTime() - startTime) / 1000;
    updateSubsidiesCounter(secondsSincePageload, widgetId);
}

function updateKillCounter(seconds, widgetId = "") {
    let animalsKilledPerYear;

    switch (window['vfasft_settings' + widgetId].region) {
        case 'vfasft-germany': animalsKilledPerYear = animalsKilledPerYearGermany; break;
        default: animalsKilledPerYear = animalsKilledPerYearWorldwide; break;
    }

    let needsSorting = true;

    for (let subset in animalsKilledPerYear) {
        const numberPerYear = animalsKilledPerYear[subset];

        const rowString = "#vfasft-row-" + subset + widgetId;
        const idString = "#vfasft-" + subset + widgetId;
        const IdStringCompact = "#vfasft-" + subset + "-compact" + widgetId;

        if (numberPerYear === 0) {
            jQuery(rowString).hide();
            continue;
        }

        const number = numberPerYear / SECONDS_PER_YEAR * seconds;
        const numberRounded = Math.ceil(number);
        const numberString = new Intl.NumberFormat('de').format(numberRounded);

        let compactString = numberString;
        if (numberRounded > 999999) {
            compactString = new Intl.NumberFormat('de', { notation: "compact", compactDisplay: "long" }).format(numberRounded);
        }

        // Insert number as data which to sort the list by.
        // If the data already exists, the list was sorted before, nothing to do then.
        if (typeof jQuery(rowString).attr('data-number') !== 'undefined') {
            needsSorting = false;
        }
        else {
            jQuery(rowString).attr('data-number', numberPerYear);
        }

        jQuery(idString).text(numberString);
        jQuery(IdStringCompact).html(compactString);
    }
    if (needsSorting) {
        jQuery("#vfasft-list" + widgetId).each(function () {
            jQuery("#vfasft-list" + widgetId).html(jQuery(this).children('li:visible').sort(function (a, b) {
                return parseInt(jQuery(b).attr('data-number')) < parseInt(jQuery(a).attr('data-number')) ? -1 : 1;
            }));
        });
    }
}

function timerangeSelected(item) {
    const widgetId = item.attr('data-widget-id');

    if (item.hasClass('vfasft-tab-live')) {
        if (widgetInstances[widgetId].intervalHandle === undefined) {
            selectTabContent('live', widgetId);
            chooseAndStartTimer(widgetId);
        }
    }
    else {
        const intervalHandle = widgetInstances[widgetId].intervalHandle;
        let seconds = 0;

        if (intervalHandle !== undefined) {
            clearInterval(intervalHandle);
            // delete intervalHandle; <- Note: This does not work
            delete widgetInstances[widgetId].intervalHandle;
        }

        if (item.hasClass('vfasft-tab-second')) { seconds = 1; selectTabContent('second', widgetId); }
        else if (item.hasClass('vfasft-tab-day')) { seconds = SECONDS_PER_DAY; selectTabContent('day', widgetId); }
        else if (item.hasClass('vfasft-tab-week')) { seconds = SECONDS_PER_WEEK; selectTabContent('week', widgetId); }
        else if (item.hasClass('vfasft-tab-month')) { seconds = SECONDS_PER_MONTH; selectTabContent('month', widgetId); }
        else if (item.hasClass('vfasft-tab-year')) { seconds = SECONDS_PER_YEAR; selectTabContent('year', widgetId); }

        switch (widgetInstances[widgetId].mode) {
            case "vfasft-killcounter": updateKillCounter(seconds, widgetId); break;
            case "vfasft-subsidies": updateSubsidiesCounter(seconds, widgetId); break;
            default: break;
        }
    }
}

function selectTabContent(timerange, widgetId) {
    jQuery(".vfasft-tab[id$='" + widgetId + "']").hide();
    jQuery("#vfasft-tab-content-" + timerange + widgetId).css('display', 'block');
}

function updateSubsidiesCounter(seconds, widgetId = "") {
    const number = Math.round(subsidiesPerYear / SECONDS_PER_YEAR * seconds);

    const numberString = new Intl.NumberFormat('de').format(number);
    jQuery("#vfasft-subsidies-number" + widgetId).text(numberString);

    let compactString = "";
    if (number > 999999) {
        compactString = " (" + new Intl.NumberFormat('de', { notation: "compact", compactDisplay: "long" }).format(number) + ")";
    }
    jQuery("#vfasft-subsidies-compact" + widgetId).text(compactString);
}

jQuery("input.vfasft-input").click(function () {
    timerangeSelected(jQuery(this));
});

setInterval(function () {
    const currentTime = new Date().getTime();
    const diffInSeconds = (currentTime - startTime) / 1000;
    const diffInMinutes = diffInSeconds / 60;

    const seconds = Math.floor(diffInSeconds % 60);
    const minutes = Math.floor(diffInMinutes % 60);
    const hours = Math.floor(diffInMinutes / 60);
    if(hours > 0){
        jQuery('.vfasft-clock-hours').show();
        jQuery('.vfasft-clock-hours-number').text(hours);
    }
    if(minutes > 0){
        jQuery('.vfasft-clock-minutes').show();
        jQuery('.vfasft-clock-minutes-number').text(minutes);
    }
    jQuery('.vfasft-clock-seconds-number').text(seconds);
}, 1000);
