<?php

define ('VERSION', '0.1');

function version_id() {
    if ( WP_DEBUG )
        return time();
    return VERSION;
}

function vfasft_add_scripts(){
    wp_enqueue_style('vfasft-main-style',plugins_url().'/veganforanimals-plugin/css/style.css', '', version_id());
    wp_enqueue_script("jquery");
    wp_register_script('vfasft-count-script', plugins_url().'/veganforanimals-plugin/js/count.js', '', version_id());
  }

function vfasft_add_admin_scripts(){
    wp_enqueue_script('vfasft-admin-script', plugins_url().'/veganforanimals-plugin/js/admin_page.js', '', version_id());
    wp_enqueue_style('vfasft-admin-style',plugins_url().'/veganforanimals-plugin/css/admin_page.css', '', version_id());
}
  
add_action('admin_enqueue_scripts', 'vfasft_add_admin_scripts');
add_action('wp_enqueue_scripts', 'vfasft_add_scripts');
?>
